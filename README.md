# Monolith-to-Microservices Welcome

This project will help make sure your development environment is ready
for the class. Please go through these steps prior to day 1,
especially since the first run of Maven can take quite a long time to
download JAR files.

# Set JAVA_HOME

Please make sure the JAVA_HOME environment variable is set to a Java 8
JDK. Both Oracle JDK and OpenJDK will work. If you're adventurous, you
can try the GraalVM.

# Initialize Maven

This repository has "Maven wrapper" scripts for Unix-like and Windows
dev machines. Please run one of these to make sure you have the
necessary JARs downloaded.

Windows: `mvnw.cmd spring-boot:run`

Mac/Linux/WSL: `./mvnw spring-boot:run`

# Verify Network Connections

Make sure you can connect to your own host via localhost. After
starting the welcome project above, visit
[http://localhost:8080/welcome](http://localhost:8080/welcome) to see
a friendly greeting.

# Get Go-ing

We will be using Go for a portion of the class. Please visit the
[Go download page](https://golang.org/dl/) and download the
appropriate distribution.

You may also want to take [the Go
tour](https://tour.golang.org/welcome/1) but it isn't a
requirement. You'll be provided with most of the necessary code.

The Go portions have been tested on Mac, Linux, Windows, and WSL.

# Install NodeJS

We will also use NodeJS for a portion of the class. Please visit the
[NodeJS](https://nodejs.org/en/) download page and grab a 10.x
release.

# Terraform

Finally, we will use [Terraform](https://www.terraform.io/) to set up
our AWS environments. It would be helpful to [download and
install](https://www.terraform.io/downloads.html) Terraform before
class begins.
