Dear learner,

You are signed up for the "Monolith to Microservices" workshop
starting shortly. This will be a busy, fun, and instructive week of
hands-on development and operations.

This email has some practical information that will help you set up
for a successful class.

# Daily Schedule

The workshop begins at 9 am each day. The instructor will be present
before that to handle any pending questions or technical issues you
might have from the previous day.

We will end at 4 pm each day. At that time, you are free to leave the
class room. However, if you need help or want to discuss anything, the
instructor will be available until 5 pm each day.

We will not have formal break times. However, we will have frequent
working sessions. You can use any part of the "hands-on" time to
attend to urgent issues.

# Tools and Languages

During this class, we'll be working with a number of tools that you
may or may not already have installed. Please take a few minutes to
review this list and set up your environment so you can get the most
out of your class time.

## Ruby on Rails

Our initial monolithic application is built with [Ruby on
Rails](http://rubyonrails.org/). If you
haven't worked on a Rails app before, you might find the [Getting
Started](http://guides.rubyonrails.org/getting_started.html) guide
helpful.

The Ruby community has evolved several different ways to manage your
development environment. Many Ruby developers like to isolate each
project and not install "gems" into their system environment. (It
reduces dependency conflicts between different projects.) Our starter
code base is set up for [rbenv](https://github.com/rbenv/rbenv) so you
might find that helpful.

If you are on a Mac, and you are _not_ a Ruby developer, it is likely
that your Ruby installation is out of date. Please do install rbenv,
[ruby-build](https://github.com/rbenv/ruby-build#readme) and then run
`rbenv install 2.3.3` to get a version that will work with our monolith.

## Terraform

We will use [Terraform](http://terraform.io) to automate our
infrastructure creation. Please download the Terraform binary and
place it somewhere on your path.

## Go

One of our services will be written in [Go](https://golang.org/). Take
a few minutes to [download](https://golang.org/dl/) the right
distribution for your system.

You don't need to know Go before the class. Code samples will help you
get through. But if you want to learn more about it, [a Tour of
Go](https://tour.golang.org/welcome/1) is a quick, hands-on
introduction to the language.

## Node

Another service will use the very popular
[Node](https://nodejs.org/en/) framework for Javascript. You will want
to download and install this before class.

## Java

We will also use [Spring
Boot](https://projects.spring.io/spring-boot/) and [Java
10](http://jdk.java.net/10/). Spring Boot is a set of jar files, so
there's nothing you need to install for that. You should make sure to
install JDK 10 before class.

You can use any Java IDE you like, or none at all.

The first time you run Maven, it can take a long time to download jar
files. The [m2m.welcome](https://gitlab.com/mtnygard/m2m.welcome)
project will help you preload those files. It has "Maven wrapper"
scripts for Unix-like and Windows dev machines. Run one of
these to make sure you have the necessary jars downloaded.

Windows: `mvnw.cmd spring-boot:run`

Mac/Linux/WSL: `./mvnw spring-boot:run`

To verify network configuration, make sure you can connect to your own
host via localhost. After starting the welcome project above, visit
[http://localhost:8080/welcome](http://localhost:8080/welcome) to see
a friendly greeting.

## That's a lot of languages!

Why so many languages? Polyglot programming is a reality in
microservices. Even in organizations that are dedicated to Java, Ruby,
or C#, you'll find bits and pieces in other languages. We all need to
be comfortable reading code that isn't in our native language.

These three languages will also give you a good basis for comparison
to decide which tool you want to apply to which situation.

# Accounts and Access Control

When building microservices, authentication can be half the
battle. Getting the rights to deploy code, set up databases, and make
service calls can be finicky and time-consuming.

To help the class run smoothly, please take a few minutes to follow
these instructions.

## AWS Access

As part of the class setup, a new AWS user will be created for
you. This user will have the necessary permission for everything in
our class. It will _not_ be part of your regular AWS account and
environment, so it's a totally safe environment.

You'll use an SSH private key to log in to your hosts. You might want
to create a new key just for this class. Please send the public half
of the key to [Michael
Nygard](mailto:michael.nygard@n6consulting.com).

## Gitlab

We will use [Gitlab](https://gitlab.com) for version control. If
you're familiar with Github, Gitlab is very similar to that. The main
difference is that a free account can create unlimited repositories.

We'll use Gitlab's version control and continuous integration
features. Please set up a free Gitlab account.

You'll need to configure an [SSH key](https://gitlab.com/profile/keys)
for your Gitlab account. This can be the same key you created for AWS
access or it can be a new one.

---

Here's looking forward to a great workshop!

Michael Nygard
